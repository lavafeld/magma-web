---
title: Magma guide release announcement
date: '2020-01-29'
image: "img/magma-logo.svg"
author: "Vasilis Ververis"
tags: ["magma-guide", "release"]
---

We are very pleased to announce you that the magma guide has been released.

**What is the magma guide?**

An open-licensed, collaborative repository that provides the first publicly
available research framework for people working to measure information controls
and online censorship activities. In it, users can find the resources they need
to perform their research more effectively and efficiently.

It is available under the following website:
https://magma.lavafeld.org

The content of the guide represents industry best practices, developed in
consultation with networking researchers, activists, and technologists. And it's
evergreen, too--constantly updated with new content, resources, and tutorials.
The [host website](https://magma.lavafeld.org) is regularly updated and synced
to a version control repository (Git) that can be used by members of the network
measurements community to review, translate, and revise content of the guide.

If you or someone you know is able to provide such information, please
[get in touch with us](/contact/) or read on how you can directly
[contribute](https://magma.lavafeld.org/guide/contribute.html) to the guide.

All content of the magma guide (unless otherwise mentioned) is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)

Many thanks to everyone who helped make the magma guide a reality.

You may use any of the communication channels (listed in
[contact page](/contact/)) to get in touch with us.
