---
title: "Magma guide"
image: "img/magma-logo.svg"
---

**Qué es la guía de magma?**

Un repositorio colaborativo de licencia abierta que proporciona la primera marco
de investigación disponible para las personas que trabajan en la medición de
controles de información y actividades de censura en línea. En ella, los
usuarios pueden encontrar los recursos que necesitan para llevar a cabo sus
investigaciones de manera más efectiva y eficiente.

Está disponible en el siguiente sitio web:
https://magma.lavafeld.org
